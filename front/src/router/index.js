import Vue from 'vue'
import Router from 'vue-router'
import OnStock from '@/components/OnStock'
import NewProduct from '@/components/NewProduct'
import Received from '@/components/Received'
import Sold from '@/components/Sold'
import NewReport from '@/components/NewReport'
import Report from '@/components/Report'

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: "/onstock",
            component: OnStock,
            name: "onstock",
        },
        {
            path: "/product/new",
            component: NewProduct,
            name: "newproduct",
        },
        {
            path: "/received",
            component: Received,
            name: "received",
        },
        {
            path: "/sold",
            component: Sold,
            name: "sold",
        },
        {
            path: "/report/new",
            component: NewReport,
            name: "reportnew",
        },
        {
            path: "/report",
            component: Report,
            name: "report",
        },
    ]
});


export default router
