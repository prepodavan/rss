import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const state = {
    report: [],
};

const mutations = {
    setReport(state, report) {
        state.report = report.products;
    },
};

export default new Vuex.Store({
    state: state,
    mutations: mutations,
});