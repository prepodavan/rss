﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using rss_stock.Models.DbModels;
using rss_stock.Models.DbModels.Repos;
using ApiModels = rss_stock.Models.ApiModels;

namespace rss_stock.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductRepo productRepo;

        public ProductsController(IProductRepo productRepo)
        {
            this.productRepo = productRepo;
        }

        [HttpGet("report")]
        public async Task<ActionResult<IEnumerable<ApiModels.Product>>> GetReport(
            [FromQuery] ReportQueryParams args)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            List<Status.StatusCode> statusCodes;
            try
            {
                statusCodes = args.StatusesList
                    .AsEnumerable()
                    .Select(str => Enum.Parse<Status.StatusCode>(str))
                    .ToList();
            }
            catch (ArgumentException)
            {
                return new JsonResult(new { Reason = "wrong status string" })
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }

            var productStatuses = await productRepo.GetAll(args.StartDT, args.EndDT, statusCodes);
            var products = productStatuses
                .AsEnumerable()
                .Select(ps => new ApiModels.Product(ps));
            return new JsonResult( new { Products = products });
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ApiModels.Product>>> GetProducts(
            [FromQuery(Name = "page")] int page,
            [FromQuery(Name = "status")] string status
            )
        {
            Status.StatusCode code;
            try
            {
                code = Enum.Parse<Status.StatusCode>(status);
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }

            var dbProducts = await productRepo.GetAll(page, code);
            var apiProducts = dbProducts
                .AsEnumerable()
                .Select(pr => new ApiModels.Product(pr));
            return Ok(apiProducts);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ApiModels.Product>> GetProduct(long id)
        {
            try
            {
                ProductStatus ps = await productRepo.GetProduct(id);
                return new ApiModels.Product(ps);
            }
            catch (ProductNotFoundException)
            {
                return NotFound();
            }
        }
        

        [HttpPut("{productId}/status")]
        public async Task<IActionResult> ChangeProductStatus(long productId, [FromBody] ApiModels.Status.InputSchema inputSchema)
        {
            Status.StatusCode statusCode;
            try
            {
                statusCode = Enum.Parse<Status.StatusCode>(inputSchema.Name);
            }
            catch (ArgumentException)
            {
                return new JsonResult(new { Reason = "no such status" })
                {
                    StatusCode = StatusCodes.Status400BadRequest
                };
            }

            ProductStatus ps;
            try
            {
                ps = await productRepo.GetProduct(productId);
            }
            catch (ProductNotFoundException)
            {
                return new JsonResult(new { Reason = "no such product" })
                {
                    StatusCode = StatusCodes.Status404NotFound
                };
            }

            var currentStatus = (ushort) Enum.Parse<Status.StatusCode>(ps.Status.Name);
            if ( (ushort)statusCode <= currentStatus)
            {
                return new JsonResult(new { Reason = "wrong status" })
                {
                    StatusCode = StatusCodes.Status409Conflict
                };
            }

            await productRepo.SetStatus(ps, statusCode);
            return new OkResult();
        }

        [HttpPost]
        public async Task<ActionResult<ApiModels.Product>> PostProduct([FromBody] ApiModels.Product.CreationParams data)
        {
            try
            {
                var ps = await productRepo.CreateProduct(data.Name, data.Price);
                return CreatedAtAction("GetProduct", new { id = ps.ProductId }, new ApiModels.Product(ps));
            } catch (SystemException)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<ApiModels.Product>> DeleteProduct(long id)
        {
            try
            {
                var ps = await productRepo.Remove(id);
                return new ApiModels.Product(ps);
            }
            catch (ProductNotFoundException)
            {
                return NotFound();
            }
        }

        public class ReportQueryParams
        {
            [BindRequired]
            public long Start { get; set; }
            
            [BindRequired]
            public long End { get; set; }
            public DateTime StartDT 
            { 
                get => DateTimeOffset.FromUnixTimeSeconds(Start).DateTime;
            }

            public DateTime EndDT
            {
                get => DateTimeOffset.FromUnixTimeSeconds(End).DateTime;
            }

            [BindRequired]
            [MinLength(1)]
            public string Statuses { get; set; }

            public List<string> StatusesList
            {
                get => Statuses.Split("|").ToList();
            }
        }
    }
}
