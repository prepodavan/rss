﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DbModels = rss_stock.Models.DbModels;
using System.ComponentModel.DataAnnotations;

namespace rss_stock.Models.ApiModels
{
    public class Product
    {
        public Product(DbModels.ProductStatus ps)
        {
            Id = ps.Product.Id;
            Name = ps.Product.Name;
            Price = ps.Product.Price;
            Status = new Status()
            {
                Name = ps.Status.Name,
                UpdatedAt = ps.UpdatedAt ?? ps.CreatedAt
            };
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Status Status { get; set; }

        public class CreationParams
        {
            [Required(ErrorMessage = "Укажите имя товара")]
            public string Name { get; set; }
            
            [Required(ErrorMessage = "Укажите цену товара")]
            public decimal Price { get; set; }
        }
    }
}
