﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rss_stock.Models.ApiModels
{
    public class Status
    {
        public string Name { get; set; }
        public DateTime UpdatedAt { get; set; }
        public class InputSchema
        {
            public string Name { get; set; }
        }
    }
}
