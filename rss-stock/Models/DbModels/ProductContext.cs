﻿using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace rss_stock.Models.DbModels
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options) : base(options)
        {
            Database.EnsureCreated();
            ChangeTracker.StateChanged += ChangeTracker_StateChanged;
            EnsureStatusesCreated();
        }

        private void EnsureStatusesCreated()
        {
            var names = Enum.GetNames(typeof(Status.StatusCode));
            foreach (string name in names)
            {
                Status status = Statuses
                    .Where(st => st.Name == name)
                    .SingleOrDefault();
                if (status == null)
                {
                    status = new Status() { Name = name };
                    Statuses.Add(status);
                }
            }
            SaveChanges();
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<ProductStatus> ProductStatuses { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Status>().HasQueryFilter(p => p.DeletedAt == null);
            builder.Entity<Product>().HasQueryFilter(p => p.DeletedAt == null);
            builder.Entity<ProductStatus>().HasQueryFilter(p => p.DeletedAt == null);

            var status = builder.Entity<Status>();
            status
                .HasIndex(st => st.Name)
                .IsUnique();
            var ps = builder.Entity<ProductStatus>();
            ps.HasOne(ps => ps.Status);
            ps.HasOne(ps => ps.Product);
            ps
                .HasIndex(ps => new { ps.ProductId, ps.DeletedAt })
                .IsUnique();
        }
        private void ChangeTracker_StateChanged(object sender, EntityStateChangedEventArgs e)
        {
            if(e.Entry.Entity is IEntity && e.Entry.State == EntityState.Modified)
            {
                var entity = ((IEntity)e.Entry.Entity);
                entity.UpdatedAt = DateTime.Now;
            }
        }
    }
}
