﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace rss_stock.Models.DbModels
{
    public partial class ProductStatus : BaseEntity
    {
        public long ProductId { get; set; }
        public virtual Product Product { get; set; }
        public virtual Status Status { get; set; }
    }
}
