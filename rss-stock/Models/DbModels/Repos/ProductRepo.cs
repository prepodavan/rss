﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using rss_stock.Models.DbModels;

namespace rss_stock.Models.DbModels.Repos
{
    public interface IProductRepo
    {
        Task<ProductStatus> CreateProduct(string name, decimal price);
        Task<ProductStatus> GetProduct(long id);
        Task<ProductStatus> Remove(long id);
        Task SetStatus(long id, Status.StatusCode status);
        Task SetStatus(ProductStatus ps, Status.StatusCode status);
        Task<ICollection<ProductStatus>> GetAll(int page, Status.StatusCode status);
        Task<ICollection<ProductStatus>> GetAll(DateTime start, DateTime end, ICollection<Status.StatusCode> statuses);
    }
    public class ProductRepo : IProductRepo
    {
        private readonly int pageSize = 20;
        private readonly ProductContext db;

        public ProductRepo(ProductContext context)
        {
            db = context;
        }

        public async Task<ProductStatus> CreateProduct(string name, decimal price)
        {
            var status = await db
               .Statuses
               .Where(st => st.Name == Status.StatusCode.Received.ToString())
               .SingleOrDefaultAsync();

            if (status == null)
            {
                throw new SystemException("no needed status in db");
            }

            var product = new Product()
            {
                Price = price,
                Name = name
            };

            var productStatus = new ProductStatus()
            {
                Product = product,
                Status = status,
            };

            db.Products.Add(product);
            db.ProductStatuses.Add(productStatus);
            await db.SaveChangesAsync();
            return productStatus;
        }

        public async Task<ICollection<ProductStatus>> GetAll(int page, Status.StatusCode status)
        {
            return await db
                .ProductStatuses
                .OrderBy(ps => ps.Id)
                .Include(ps => ps.Product)
                .Include(ps => ps.Status)
                .Where(ps => ps.Status.Name == status.ToString())
                .Skip(page * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public async Task<ICollection<ProductStatus>> GetAll(DateTime start, DateTime end, ICollection<Status.StatusCode> statuses)
        {
            var arrStatuses = statuses
                .AsEnumerable()
                .Select(st => st.ToString())
                .ToArray();

            return await db
                .ProductStatuses
                .Include(ps => ps.Product)
                .Include(ps => ps.Status)
                .Where(ps => arrStatuses.Contains(ps.Status.Name))
                .Where(ps => ps.CreatedAt >= start && ps.CreatedAt <= end 
                && (ps.UpdatedAt == null || (ps.UpdatedAt >= start && ps.UpdatedAt <= end)))
                .ToListAsync();
        }

        private bool IsInRange(ProductStatus ps, DateTime start, DateTime end)
        {
            bool created = ps.CreatedAt <= start && ps.CreatedAt <= end;
            bool updated = ps.UpdatedAt == null ? true : ps.UpdatedAt >= start && ps.UpdatedAt <= end;
            return created && updated;
        }

        public async Task<ProductStatus> GetProduct(long id)
        {
            var product = await db
                .ProductStatuses
                .Where(_ps => _ps.ProductId == id)
                .Include(_ps => _ps.Product)
                .Include(_ps => _ps.Status)
                .SingleOrDefaultAsync();
            return product ?? throw new ProductNotFoundException();
        }

        public async Task<ProductStatus> Remove(long id)
        {
            ProductStatus ps = await db
                .ProductStatuses
                .Where(_ps => _ps.ProductId == id)
                .Include(_ps => _ps.Product)
                .Include(_ps => _ps.Status)
                .SingleOrDefaultAsync();

            if (ps == null)
            {
                throw new ProductNotFoundException();
            }

            ps.Product.DeletedAt = DateTime.Now;
            ps.DeletedAt = DateTime.Now;
            await db.SaveChangesAsync();
            return ps;
        }
        public async Task SetStatus(ProductStatus ps, Status.StatusCode statusCode)
        {
            Status status = await db
                .Statuses
                .Where(st => st.Name == statusCode.ToString())
                .SingleOrDefaultAsync();

            if (status == null)
            {
                throw new SystemException("no such status in db");
            }

            ps.Status = status;
            db.Entry(ps).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }
        public async Task SetStatus(long id, Status.StatusCode statusCode)
        {
            ProductStatus ps = await db
                .ProductStatuses
                .Where(_ps => _ps.ProductId == id)
                .Include(_ps => _ps.Product)
                .SingleOrDefaultAsync();

            if (ps == null)
            {
                throw new ProductNotFoundException();
            }

            await SetStatus(ps, statusCode);
        }
    }
}
