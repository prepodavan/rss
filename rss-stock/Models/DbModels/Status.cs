﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace rss_stock.Models.DbModels
{
    public partial class Status : BaseEntity
    {
        public string Name { get; set; }
        public enum StatusCode : ushort
        {
            Received = 0,
            OnStock = 1,
            Sold = 2
        }
    }
}
