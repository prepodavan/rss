using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using rss_stock.Controllers;
using rss_stock.Models.DbModels;
using rss_stock.Models.DbModels.Repos;

namespace rss_stock
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddDbContext<ProductContext>(opt => opt
                    .UseSqlite("Filename=products.sqlite3"))
                    //.UseInMemoryDatabase("products"))
                .AddScoped<IProductRepo, ProductRepo>()
                .AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var staticFolder = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "..", "..", "..", "..", "front", "dist"));
            app
                .UseHttpsRedirection()
                .UseRouting()
                .UseAuthorization()
                .UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(staticFolder),
                    RequestPath = ""
                })
                .UseEndpoints(endpoints => {
                    endpoints.MapControllers();
                });
        }
    }
}
